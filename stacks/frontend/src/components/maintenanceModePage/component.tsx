import * as React from 'react';
import { OutboundLink } from 'react-ga';

import { PageMetadata } from '../pageMetadata/component';

export const MaintenanceModePage = () => (
  <div>
    <header className="hero is-medium is-warning">
      <div className="hero-body">
        <div className="container">
          <h1 className="title has-text-weight-normal">Flockademic is down for maintenance</h1>
          <PageMetadata url="https://Flockademic.com" title="Flockademic is down for maintenance" />
        </div>
      </div>
    </header>
    <section className="section">
      <div className="container">
        {/* tslint:disable-next-line:max-line-length */}
        Unfortunately, Flockademic is currently down for planned maintenance. Not to worry though, it will be back up soon! Keep an eye on&nbsp;
        <OutboundLink
          to="https://twitter.com/Flockademic"
          title="Flockademic on Twitter"
          eventLabel="twitter"
        >
          @Flockademic on Twitter
        </OutboundLink>
        &nbsp;for updates.
      </div>
    </section>
  </div>
);
