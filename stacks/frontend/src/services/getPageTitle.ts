export function getPageTitle(pageName?: string) {
  if (!pageName) {
    return 'Flockademic (beta): More impact with your research';
  }

  return `${pageName} | Flockademic (beta)`;
}
