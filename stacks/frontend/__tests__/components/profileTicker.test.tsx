import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const mockProfiles = [
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
];

jest.mock('../../src/services/crossref', () => ({
  getOrcidAuthors: jest.fn().mockReturnValue(Promise.resolve(mockProfiles)),
}));

import { ProfileTicker } from '../../src/components/profileTicker/component';

it('should not render anything when there was an error fetching profiles', (done) => {
  const mockCrossrefService = require.requireMock('../../src/services/crossref');
  mockCrossrefService.getOrcidAuthors.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const overview = shallow(<ProfileTicker />);

  setImmediate(() => {
    overview.update();

    expect(overview).toBeEmptyRender();

    done();
  });
});

it('should not render anything when there are fewer than five profiles', (done) => {
  const fewMockProfiles = [ mockProfiles[0], mockProfiles[1], mockProfiles[2], mockProfiles[3] ];
  const mockCrossrefService = require.requireMock('../../src/services/crossref');
  mockCrossrefService.getOrcidAuthors.mockReturnValueOnce(Promise.resolve(fewMockProfiles));

  const overview = shallow(<ProfileTicker />);

  setImmediate(() => {
    overview.update();

    expect(overview).toBeEmptyRender();

    done();
  });
});

it('should match the snapshot with proper data', (done) => {
  const mockCrossrefService = require.requireMock('../../src/services/crossref');
  mockCrossrefService.getOrcidAuthors.mockReturnValueOnce(Promise.resolve(mockProfiles));

  const overview = shallow(<ProfileTicker />);

  setImmediate(() => {
    overview.update();

    expect(overview).toMatchSnapshot();

    done();
  });
});
