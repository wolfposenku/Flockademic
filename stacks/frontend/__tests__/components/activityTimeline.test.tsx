import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ActivityTimeline } from '../../src/components/activityTimeline/component';

const mockProps = {
  articles: [
    {
      author: [
        {
          identifier: 'arbitrary-id',
          name: 'Prostetnic Vogon Jeltz',
          sameAs: 'https://orcid.org/0000-0002-4013-9889',
        },
      ],
      datePublished: 'arbitrary date',
      // tslint:disable-next-line:max-line-length
      description: 'In this article we discuss different approaches to calculating the optimal routes for intergalactic bypasses.',
      identifier: 'arbitrary-id',
      name: 'Route optimisations for intergalactic bypasses',
    },
    {
      author: [
        { identifier: 'arbitrary-other-id', name: 'Mr L Prosser' },
      ],
      datePublished: 'arbitrary date',
      // tslint:disable-next-line:max-line-length
      identifier: 'arbitrary-id',
      name: 'On the relocation of human debris',
    },
  ],
};

it('should not render anything when there are no articles', () => {
  const mockArticles = [];

  const overview = shallow(<ActivityTimeline {...mockProps} articles={mockArticles} />);

  expect(overview).toBeEmptyRender();
});

it('should display article names and their authors', () => {
  const overview = shallow(<ActivityTimeline {...mockProps} />);

  expect(toJson(overview)).toMatchSnapshot();
});
