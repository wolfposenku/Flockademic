import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function addFileToScholarlyArticle(
  database: Database,
  session: Session,
  articleId: string,
  url: string,
  filename: string,
): Promise<null> {
  interface ArticleRow {
    identifier: string;
    name: string;
    description: string;
    creator_session: string;
    author: string;
  }
  const result = await database.one<ArticleRow>(
    'SELECT article.identifier, article.name, article.description, article.creator_session, author.author'
      + ' FROM scholarly_articles article'
      + ' LEFT JOIN scholarly_article_authors author ON author.scholarly_article=article.identifier'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' WHERE article.identifier=${articleId}'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' AND (article.creator_session=${sessionId} OR author.author=${authorId})',
    {
      articleId,
      authorId: (session.account) ? session.account.identifier : undefined,
      sessionId: session.identifier,
    },
  );

  return database.none(
    'INSERT INTO scholarly_article_associated_media(scholarly_article, content_url, name)'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' VALUES (${articleId}, ${url}, ${filename})',
    {
      articleId: result.identifier,
      filename,
      url,
    },
  );
}
